!!! IMPORTANT NOTICE !!!

The code of SMAP-Haplotype-Window is now a module of SMAP. 
This project is no longer maintained, please refer to https://gitlab.ilvo.be/genomics/smap-package/smap

-----

# SMAP-Haplotype-Window
SMAP-Haplotype-Window is a plugin for SMAP that allows for the analysis of sequencing data from CRISPR edited loci.

## Installation
Installation should be performed by installing [SMAP](https://gitlab.com/truttink/smap), and afterwards install this plugin.

```{bash}
python3 -m venv .venv
source .venv/bin/activate
pip install -U pip
pip install ngs-smap
pip install smap-haplotype-window
```


